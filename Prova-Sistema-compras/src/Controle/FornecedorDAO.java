/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.Fornecedor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author almir
 */
public class FornecedorDAO {

    Connection connection = null;

    // Insere fornecedores no BD
    public boolean inserirFornecedor(Fornecedor fornecedor) {
        System.out.println("inserirFornecedor");
        // inicia a conexao com o Banco de dados chamando
        // a classe Conexao
        connection = Conexao.getInstance().getConnection();
        System.out.println("conectado e preparando para inserir");
        String sql = "INSERT INTO fornecedor (nome,situacao,cidade) VALUES (?,?,?);";
        PreparedStatement stmt = null;
        try {
            stmt = (PreparedStatement) connection.prepareStatement(sql);

            System.out.println("SQL: " + sql);
            stmt.setString(1, fornecedor.getNome());
            stmt.setString(2, fornecedor.getSituacao());
            stmt.setString(3, fornecedor.getCidade());

            stmt.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        } finally {
            // este bloco finally sempre executa na instrução try para
            // fechar a conexão a cada conexão aberta
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                System.out.println("Erro ao desconectar" + e.getMessage());
            }
        }
    }
    // Fim Inserir Fornecedores

    // Conecta o JTable no BD e mostra os dados
    public List<Fornecedor> retornaLista() {

        connection = Conexao.getInstance().getConnection();
        System.out.println("conectado a sistemaCompras");
        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Fornecedor> fornecedores = new ArrayList<>();

        try {

            stmt = connection.prepareStatement("SELECT * FROM fornecedor;");
            rs = stmt.executeQuery();

            while (rs.next()) {

                Fornecedor f = new Fornecedor();

                f.setId(rs.getInt("id"));
                f.setNome(rs.getString("nome"));
                f.setSituacao(rs.getString("situacao"));
                f.setCidade(rs.getString("cidade"));
                fornecedores.add(f);

            }

        } catch (SQLException e) {
            System.out.println("Erro ao desconectar" + e.getMessage());
        }

        return fornecedores;

    }
    //Fim retorno da Lista de fornecedores na JTable

    // Função atualizarUsuario para atualizar (Update) do CRUD
    public boolean atualizarFornecedores(Fornecedor fornecedor) {
        System.out.println("atualizarFornecedores");
        // inicia a conexao com o Banco de dados chamando
        // a classe Conexao
        connection = Conexao.getInstance().getConnection();
        System.out.println("conectado e preparando para atualizar");
        String sql = "UPDATE fornecedor SET nome = ?, situacao = ?, cidade = ? WHERE id = ?";
        PreparedStatement stmt = null;
        try {
            stmt = (PreparedStatement) connection.prepareStatement(sql);

            System.out.println("SQL: " + sql);
            stmt.setString(1, fornecedor.getNome());
            stmt.setString(2, fornecedor.getSituacao());
            stmt.setString(3, fornecedor.getCidade());
            stmt.setInt(4, fornecedor.getId());

            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");

            stmt.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar: " + e);
            return false;
        } finally {
            // este bloco finally sempre executa na instrução try para
            // fechar a conexão a cada conexão aberta
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                System.out.println("Erro ao desconectar" + e.getMessage());
            }
        }

    }
    // Fim atualizarFornecedores

    public boolean excluirFornecedor(Fornecedor fornecedor) {
        System.out.println("excluirFornecedor");
        // inicia a conexao com o Banco de dados chamando
        // a classe Conexao
        connection = Conexao.getInstance().getConnection();
        System.out.println("conectado e preparando para excluir");
        String sql = "DELETE FROM fornecedor WHERE id = ?";
        PreparedStatement stmt = null;
        try {
            stmt = (PreparedStatement) connection.prepareStatement(sql);

            System.out.println("SQL: " + sql);
            stmt.setInt(1, fornecedor.getId());

            JOptionPane.showMessageDialog(null, "Excluido com sucesso!");

            stmt.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir: " + e);
            return false;
        } finally {
            // este bloco finally sempre executa na instrução try para
            // fechar a conexão a cada conexão aberta
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                System.out.println("Erro ao desconectar" + e.getMessage());
            }
        }

    }
    //fim func excluir

    public List<Fornecedor> BuscaLista(String econ) {

        connection = Conexao.getInstance().getConnection();
        System.out.println("conectado a sistemaCompras - Neymar, melhor do mundo");
        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Fornecedor> fornecedores = new ArrayList<>();

        try {

            stmt = connection.prepareStatement("SELECT * FROM fornecedor WHERE nome LIKE ?");
            stmt.setString(1, "%" + econ + "%");
            rs = stmt.executeQuery();

            while (rs.next()) {

                Fornecedor fornecedor = new Fornecedor();

                fornecedor.setId(rs.getInt("id"));
                fornecedor.setNome(rs.getString("nome"));
                fornecedor.setSituacao(rs.getString("situacao"));
                fornecedor.setCidade(rs.getString("cidade"));
                fornecedores.add(fornecedor);

            }

        } catch (SQLException e) {
            System.out.println("Erro ao desconectar" + e.getMessage());
        }

        return fornecedores;

    }
}
